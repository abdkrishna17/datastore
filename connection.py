import mysql.connector as conn

class Connection:
    def __init__(self):
        self.con = conn.connect(host = "localhost",database = "pythontest",user = "root",password = "Admin123")
        if self.con.is_connected():
            print("connection sucessful")
        else:
            print("Retry")

    def create_table(self,tablename,schema):
        query_create = "CREATE TABLE IF NOT EXISTS {0} ({1})".format(tablename,schema)
        cur = self.con.cursor()
        cur.execute(query_create)
        self.con.commit()
        print("Table create succesfull")

        
    def insert_into_table(self,tablename,schema,value):
        st = schema.split(',')
        str1 = ''
        for i in st:
            b = i.split(' ')
            str1 = str1+b[0]+','
        schema = str1[:len(str1)-1]
        query_insert = "INSERT INTO {0}({1} )values({2})" .format(tablename,schema,value)
        cur = self.con.cursor()
        cur.execute(query_insert)
        self.con.commit()
        print("Data Inserted")

    def select(self,tablename,select_column):
        query_select = "SELECT {1} from{0}".format(tablename,select_column)
        cur = self.con.cursor()
        cur.execute(query_select)
        row = cur.fetchall()
        for i in row:
            print(i)
            print(i)
        

    def update_table(self,tablename,set_condition,filter_condition):
        query_update = "UPDATE {0} SET {1} WHERE {2}". format(tablename,set_condition,filter_condition)
        cur =self.con.cursor()
        cur.execute(query_update)
        self.con.commit()
        print("data update")

    def delete_table(self,tablename,delete_condition):
        query_delete = "DELETE FROM {0} where id= 2".format(tablename,delete_condition)
        cur = self.con.cursor()
        cur.execute(query_delete)
        self.con.commit()
        print("DATA DELETED")



